	AREA	lib, CODE, READWRITE	
	EXPORT lab3
	EXPORT pin_connect_block_setup_for_uart0
	
U0LSR EQU 0x14			; UART0 Line Status Register

lab3
	STMFD SP!,{lr}	; Store register lr on stack
    
; Your code is placed here
	BL read_character
	BL output_character
	
	LDMFD sp!, {lr}
	BX lr
	
read_character
		STMFD SP!,{lr}	; Store register lr on stack
    
readloop	LDR r4, =0xE000C014	;Load Line Status Register
			LDRB r1, [r4]		;Load contents of LSR
			MOV r5, #1				;load 1 into r5
			AND r6, r5, r1			;Check if RDR is 1
			CMP r6, #1				
			BEQ read				;Read byte if yes
			B readloop					;try again if no
			
read		LDR r4, =0xE000C000	;Load RBR
			LDRB r0, [r4]		;Load contents of RBR
			
	 
			LDMFD sp!, {lr}
			BX lr	

output_character
			STMFD SP!, {lr}	; Store register lr on stack
								
			LDR r1, =0xE000C000		; Load address of transmit holding register in r1
outloop		LDRB r2, [r1, #U0LSR] 	; Load contents of UART LSR (address: 0xE000C014) in r2
			MOV r3, #0x20			; r3 := 0010 0000 (binary)
			AND r2, r2, r3			; r2 := (r2 && r3) [bitwise AND]
			MOV r2, r2, LSR #5		; r2 := (r2 >> 5) [log right shift 5 bits]
			CMP r2, #1				; Compare r2 with 1
			BEQ transmit			; If r2 == 1, goto "transmit"
			B outloop					; Else, (r2 != 0), goto loop
transmit	STRB r0, [r1]			; Store least-sig. byte of r0 at address r1
	 
			LDMFD sp!, {lr}
			BX lr
 
pin_connect_block_setup_for_uart0
	STMFD sp!, {r0, r1, lr}
	LDR r0, =0xE002C000  ; PINSEL0
	LDR r1, [r0]
	ORR r1, r1, #5
	BIC r1, r1, #0xA
	STR r1, [r0]
	LDMFD sp!, {r0, r1, lr}
	BX lr

	END
