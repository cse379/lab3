	AREA	lib, CODE, READWRITE	
	EXPORT lab3
	EXPORT pin_connect_block_setup_for_uart0
	
U0LSR EQU 0x14			; UART0 Line Status Register

read_character
		STMFD SP!,{lr}	; Store register lr on stack
    
loop	LDR r4, =0xE000C014	;Load Line Status Register
        LDRB r1, [r4]		;Load contents of LSR
		MOV r5, #1				;load 1 into r5
		AND r6, r5, r1			;Check if RDR is 1
		CMP r6, #1				
		BEQ read				;Read byte if yes
		B loop					;try again if no
		
read	LDR r4, =0xE000C000	;Load RBR
        LDRB r0, [r4]		;Load contents of RBR
		
 
		LDMFD sp!, {lr}
		BX lr

 
pin_connect_block_setup_for_uart0
	STMFD sp!, {r0, r1, lr}
	LDR r0, =0xE002C000  ; PINSEL0
	LDR r1, [r0]
	ORR r1, r1, #5
	BIC r1, r1, #0xA
	STR r1, [r0]
	LDMFD sp!, {r0, r1, lr}
	BX lr

	END
