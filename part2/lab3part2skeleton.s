	AREA	lib, CODE, READWRITE
	EXPORT lab3
	EXPORT pin_connect_block_setup_for_uart0

	EXPORT uart_init


U0LSR EQU 0x14				; UART0 Line Status Register
ONE_HUNDRED EQU 0x64		; Constant equal to 100 decimal

		; You'll want to define more constants to make your code easier
		; to read and debug

		; Memory allocated for user-entered strings

prompt = "This program performs signed division in the range [-999, +999] and returns the quotient and remainder. When entering dividend and divisor, please enter the appropriate sign in front of the numbers (e.g., +200 NOT 200, etc.)",0
dividendprompt = "Enter a dividend in the range [-999, +999]: ",0
divisorprompt = "Enter a divisor in the range [-999, +999]: ",0
dividend = "\0\0\0\0",0
divisor = "\0\0\0\0", 0
quotientprompt = "The quotient is: ",0
remainderprompt = "The remainder is: ",0
quotient = "\0\0\0\0",0
remainder = "\0\0\0\0",0
carreturn = 0x0D
newline = 0x0A

	ALIGN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;LAB 3;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

lab3
				STMFD SP!,{lr}			; Store register lr on stack

				LDR r4, =prompt			; loads first prompt
				BL output_string		; prints first prompt

				BL enter				; skip line on screen

				LDR r4, =dividendprompt	; loads dividend prompt
				BL output_string		; print dividend prompt

				LDR r4, =dividend		; loads dividend address
				BL read_string			; get user input

				BL enter				; skip line on screen
				;LDR r4, =dividend

				LDR r4, =divisorprompt	; loads divisor prompt
				BL output_string		; print divisor prompt

				LDR r4, =divisor		; loads divisor address
				BL read_string			; get user input

				BL enter				; skip line on screen

				LDR r4, =dividend		; load dividend address
				BL make_num				; make number from ASCII chars at address
				MOV r0, r5				; copy result into r0

				LDR r4, =divisor		; load divisor address
				BL make_num				; make number from ASCII chars at address
				MOV r1, r5				; copy result into r1

				BL div_and_mod			; Perform division of dividend by divisor
										; Quotient will be in r0, Remainder in r1
				MOV r10, r1				; Store remainder from division in r10 temporarily

				LDR r4, =quotient		; Load address of quotient string in r4
				MOV r2, r0				; r2 := quotient from division
				BL make_chars			; Convert quotient result to ASCII string

				LDR r4, =remainder		; Load address of remainder string in r4
				MOV r2, r10				; r2 := remainder from division
				BL make_chars			; Convert remainder result to ASCII string

				LDR r4, =quotientprompt	; loads quotient prompt
				BL output_string		; print quotient prompt

				LDR r4, =quotient		; loads quotient
				BL output_string		; print quotient

				BL enter				; skip line on screen

				LDR r4, =remainderprompt ; loads remainder prompt
				BL output_string		; print remainder prompt

				LDR r4, =remainder 		; loads remainder
				BL output_string		; print remainder

				BL enter


				LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;MAKE NUMBER FROM ASCII CHARS;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

make_num		STMFD SP!,{lr}			; Store register lr on stack

				LDRB r2, [r4]			; Load sign character
				CMP r2, #0x2D			; Check if sign char is '-'
				ADD r4, r4, #1			; increment address
				MOV r5, #0				; initialize running sum
				BEQ make_num_neg
				MOV r3, #0				; r3 sign flag, 0 = positive
				B not_neg				; if not negative, branch

make_num_neg	MOV r3, #1				; r3 sign flag, 1 = negative

not_neg			LDRB r2, [r4]			; Load first ASCII char digit range 0x30 - 0x39
				CMP r2, #0x00			; check if digit is null
				BEQ num_done			; if yes, done
				ADD r7, r5, r5, LSL #3	; multiply running sum by 9
				ADD r7, r5, r7			; add running sum to multiply running sum by 10
				SUB r6, r2, #0x30		; convert ASCII char to decimal digit
				ADD r5, r7, r6			; add new digit to 10x running sum
				ADD r4, r4, #1			; increment address
				B not_neg				; loop

num_done		CMP r3, #0				; Check if negative flag is 0
				BNE num_neg_1			; if yes, branch
				B go_back				; if no, done

num_neg_1		MVN r5, r5				; 1's complement running sum
				ADD r5, r5, #1			; 2's complement running sum
go_back			LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;MAKE ASCII CHARS FROM NUMBER;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Function make_chars requires the base address in memory
; where the bytes corresponding to the ASCII characters
; will be stored to be passed into the function in register
; r4.  The number to be converted must be located in
; register r2.
;
; Because the range of dividends and divisors is limited
; to [-999, 999], this also represents the range of
; values for the quotient and remainder.  Each number will
; be preceeded by the appropriate sign (e.g., +450, NOT
; 450) in memory. Zero, however, will be converted without a
; sign.) All strings are to be null-terminated,
; so five bytes maximum in memory are required to store
; the HEX values corresponding to ASCII chars: one for
; sign, up to three for digits, and one for the null char.
;
; Before the main sequence of the algorithm begins, if
; the number to be converted is Zero, we jump to the end
; of the program and store the single byte 0x30 in memory.
; Next, the sign of the number to be converted is determined.
;
; The algorithm divides the number to be converted by
; decreasing powers of 10 to isolate one digit at a time.
; The div_and_mod routine is called for this purpose.
; All zero quotients are ignored, UNTIL the first non-zero
; quotient is found. This is done to avoid converting a
; number such as 4 as "+004" (We want it to be "+4") or
; -45 as "-045".

; For example, if we want to convert the number 104 to
; ASCII characters, the algorithm would flow as follows:
;
; 1) Check if number is equal to 0: NO
;
; 2) Check sign: +
;
; 3)			78 / 100 == 0 R 78
;
; 	 We ignore the initial 0 quotient.
;
; 4) Use remainder from previous division as next dividend:
;
; 				78 / 10 == 7 R 8
;
; 	 7 + 0x30 == 0x37 (ASCII value for '7')
;	 Store 0x37 in memory at [r4]
;    Increment r4
;
; 5)			8 / 1 == 8 R 0
;
; 	 8 + 0x30 == 0x38 (ASCII value for '8')
;	 Store 0x38 in memory at [r4]
;
; 					--DONE--

make_chars		STMFD SP!,{lr}	; Store register lr on stack

				CMP r2, #0				; Compare number to zero
				BEQ num_is_zero			; If number zero, we don't need
										; a '+' or '-' sign in front, so goto "num_is_zero"
				BLT num_neg_2			; If number is negative, goto "num_neg"
										; If here, we know num is positive.
				MOV r5, #0x2B			; 0x2B is HEX for ASCII char '+'
				STRB r5, [r4]			; Store 0x2B at memory address pointed to by r4
				ADD r4, r4, #1			; Increment r4 by 1
				B num_not_neg			; Goto num_not_neg

num_neg_2		MOV r5, #0x2D			; 0x2D is HEX for ASCII char '-'
				STRB r5, [r4]			; Store 0x2D at memory address pointed to by r4
				MVN r2, r2				; Take 1s complement of r2 and store in r2
				ADD r2, r2, #1			; Complete 2s complement of r2
				ADD r4, r4, #1			; increment r4 by 1

num_not_neg
				MOV r3, #ONE_HUNDRED	; initialize r3 to 100

				MOV r6, #0				; r6 is a flag telling us if we've seen a non-zero
										; quotient so far. 0 means we have NOT, 1 means we have.

make_chars_loop
				MOV r0, r2				; Current dividend is in r2. Placed in r0 for use in div_and_mod routine
				MOV r1, r3				; Current divisor is in r3. Placed in r1 for use in div_and_mod routine

				STMFD sp!, {r2, r3, r4, r5, r6, r7, r8, r9, r10, r11} ; spill registers r2 - r11 to stack
				BL div_and_mod			; Perform division
				LDMFD sp!, {r2, r3, r4, r5, r6, r7, r8, r9, r10, r11} ; restore registers r2 - r11 from stack
				CMP r0, #0				; Compare quotient to 0
				BEQ quot_zero			; If quotient == 0, goto quot_zero

										; If here, quotient is non-zero
				MOV r6, #1				; We've seen a non-zero quot, so set flag in r6 to 1
				B process				; process quotient to ASCII char

quot_zero		CMP r6, #1				; Check if we've seen a non-zero quotient yet
				BEQ process				; If we have, don't ignore this zero quotient and process it.

										; If here, we have a zero quotient and we have not yet seen
										; a non-zero quotient, so we ignore it.

				B make_chars_update		; Goto make_chars_update


process			ADD r0, r0, #0x30		; r0 now holds HEX value of corresponding ASCII digit
				STRB r0, [r4]			; Store least significant byte of r0 in r4 (ASCII digit)
				ADD r4, r4, #1			; increment r4 by 1

make_chars_update
				MOV r5, r1				; Move remainder from division to r5 temporarily
				MOV r0, r3				; Move current divisor (a power of 10) into r0
				MOV r1, #0x0A			; r1 := 10
				STMFD sp!, {r2, r3, r4, r5, r6, r7, r8, r9, r10, r11} ; spill registers r2 - r11 to stack
				BL div_and_mod			; Divide current power of 10 by 10 to get next divisor
				LDMFD sp!, {r2, r3, r4, r5, r6, r7, r8, r9, r10, r11} ; restore registers r2 - r11 from stack
				MOV r3, r0				; New divisor is quotient from division by 10
				MOV r2, r5				; New dividend was remainder from previous division of r2 / r3, was stored in r5
				CMP r3, #0				; Compare next dividend with 0
				BEQ make_chars_done		; This will occur only after we have had a divisor of 1 and updated the divisor to
										; 0 by doing 1 / 10 = 0 R 1. At this point, we exit the loop because the last power
										; of 10 we use is 1.  If we were to continue from here, division by 0 would result.

				B make_chars_loop		; Loop through algorithm again

num_is_zero		MOV r0, #0x30			; r0 := ASCII HEX for Zero
				STRB r0, [r4]			; Store 0x30 at address pointed to by r4
				B make_chars_done		; If the number we converted was 0, we are done.
										; Goto make_chars_done

make_chars_done
				LDMFD sp!, {lr}
				BX lr
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;OUTPUT STRING;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

output_string

				STMFD SP!,{lr}			; Store register lr on stack

out_str_loop	LDRB r0, [r4]			; Load least significant byte of r4
				CMP r0, #0x00			; Check if null
				BEQ done

				BL output_character		; Goto output_character
				ADD r4, r4, #1			; Increment Address
				B out_str_loop			; Continue reading characters

done			LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;READ STRING;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

read_string
				STMFD SP!,{lr}			; Store register lr on stack

readstrloop		BL read_character		; read character
				CMP r0, #0x0D			; check if enter key pressed
				BEQ strdone				; if yes, stop reading

				STRB r0, [r4]			; store current character
				BL output_character		; print to screen
				ADD r4, r4, #1			; increment address
				B readstrloop			; rinse and repeat

strdone			LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;OUTPUT CHAR;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

output_character
				STMFD SP!,{lr}			; Store register lr on stack

				LDR r1, =0xE000C000		; Load address of transmit holding register in r1
loop			LDRB r2, [r1, #U0LSR] 	; Load contents of UART LSR (address: 0xE000C014) in r2
				MOV r3, #0x20			; r3 := 0010 0000 (binary)
				AND r2, r2, r3			; r2 := (r2 && r3) [bitwise AND]
				MOV r2, r2, LSR #5		; r2 := (r2 >> 5) [log right shift 5 bits]
				CMP r2, #1				; Compare r2 with 1
				BEQ transmit			; If r2 == 1, goto "transmit"
				B loop					; Else, (r2 != 0), goto loop
transmit		STRB r0, [r1]			; Store least-sig. byte of r0 at address r1

				LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;READ CHAR;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

read_character
				STMFD SP!,{lr}			; Store register lr on stack

readcharloop	LDR r7, =0xE000C014		; Load Line Status Register
				LDRB r1, [r7]			; Load contents of LSR
				MOV r5, #1				; Load 1 into r5
				AND r6, r5, r1			; Check if RDR is 1
				CMP r6, #1
				BEQ readchar			; Read byte if yes
				B readcharloop			; Try again if no

readchar		LDR r7, =0xE000C000		; Load RBR
				LDRB r0, [r7]			; Load contents of RBR


				LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;ENTER;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

enter		STMFD SP!,{lr}			; Store register lr on stack

				LDR r4, =carreturn
				BL output_string

				LDR r4, =newline
				BL output_string

				LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;DIVISION & MODULUS;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

div_and_mod
				STMFD r13!, {r2-r12, r14}

										; Your code for the signed division/mod routine goes here.
										; The dividend is passed in r0 and the divisor in r1.
										; The quotient is returned in r0 and the remainder in r1.

										; In the following code, r3 holds the quotient and r4 holds
										; the remainder until the end of the program, when the quotient
										; and remainder as placed in r0 and r1 respectively to be
										; returned in those registers to the caller.

										; To make computations easier, negative dividend and/or divisor
										; are negated first (two's complement is taken) in order to
										; compute quotient and remainder using non-negative numbers.
										; A flag is set in register r5 to denote whether the signs of the
										; dividend and quotient were the same (r5 := 1) or different
										; (r5 := 0).
										; If the signs were different, the quotient must be negated
										; (two's complement of it taken) before being returned to the
										; caller, whereas if the signs were the same, no negation is
										; needed because a positive quotient is expected.

				CMP r0, #0				; Compare dividend with 0.
				BNE non_zero			; If dividend is non-zero, goto label non-zero.
										; If we are here, then dividend equals 0, and we know
										; the quotient and remainder should both be equal to 0.
				MOV r3, #0				; Set quotient to 0 (r3 := 0).
				MOV r4, #0				; Set remainder to 0 (r4 := 0).
				B div_done				; Branch unconditionally to label "done", as no computations
										; are necessary.

non_zero		BGT dividendGTZero		; If dividend is greater than zero, goto label "dividendGTZero".
										; At this point, we know dividend<0.
										; To make computations easier, we take the 2s complement of the
										; dividend, then "append" negative signs later if needed.

				MVN r0, r0				; Take the 1s complement of the dividend and store it in r0.
				ADD r0, r0, #1			; Add 1 to the 1s complement of the dividend to form
										; the 2s complement.

				CMP r1, #0				; Compare the divisor to 0.
				BGT divisorGTZero		; If the divisor is greater than 0, goto label "divisorGTZero".

										; As with the dividend, here the divisor < 0.
										; Take 2s complement to make
										; computations easier, then tidy up negative sign details later.
				MVN r1, r1				; Take the 1s complement of the divisor and store it in r1.
				ADD r1, r1, #1			; Add 1 to the 1s complement of the divisor to form
										; the 2s complement.

										; Register r5 is used as a flag to let the program know
										; whether we need to worry about negative signs in the
										; final answer returned to the caller.
										; If r5==1, the quotient does NOT need to be changed before being
										; returned to caller.  If r5==0, we need to take
										; the 2s complement of the quotient before returning
										; it to the caller.
				MOV r5, #1				; Set flag in r5 ( r5 := 1 ).
				B div_continue			; Branch unconditionally to label "continue".

divisorGTZero 	MOV r5, #0				; Here, we know divisor > 0 (and dividend < 0). We set flag
										; in r5 ( r5 := 0 ). This means we will need to take the 2s
										; complement of the quotient at a later time.
				B div_continue			; Branch unconditionally to label "continue".

dividendGTZero 	CMP r1, #0				; Compare the divisor in r1 to 0.
				BGT divisorGTZero1		; Branch to "divisorGTZero1" if divisor > 0 (r1 > 0).
				MVN r1, r1				; Here we know that divisor < 0 so we negate the divisor
										; by taking 1's complenent using MVN, then
										; two's complement by adding 1 to 1's comp.
				ADD r1, r1, #1			; r1: = r1 + 1
				MOV r5, #0				; Because dividend > 0 and divisor < 0, we have opposite signs,
										; so we need to set the flag in r5 to 0 to let us know to negate
										; the quotient before returning it to caller.
				B div_continue			; Branch unconditionally to label "continue".

divisorGTZero1	MOV r5, #1				; Here we know that dividend > 0 and divisor > 0, so we set
										; r5 to 1 because we do NOT need to negate the quotient later
										; (signs of dividend and divisor are the same).

										; Beginning of implementation of given division algorithm
										; from Dr. Schindler's Microprocessors textbook, page 41.

div_continue
				MOV r2, #15				; r2 is counter, initialize to 15
				MOV r3, #0				; r3 is quotient, initialize to 0
				MOV r1, r1, LSL #15		; Logical left shift divisor in r1 by 15 places
				MOV r4, r0				; r4 is remainder, initialize to value of dividend

div_loop		SUB r4, r4, r1			; remainder := remainder - divisor (r4 := r4 - r1)

				CMP r4, #0				; compare remainder in r4 to 0
				BGE div_no				; goto label "no" if remainder in r4 >= 0

				ADD r4, r4, r1			; remainder := remainder + divisor (r4 := r4 + r1)
				MOV r3, r3, LSL #1		; Logical left shift quotient by 1

				B div_yes				; goto label "yes" unconditionally

div_no			MOV r3, r3, LSL #1		; Logical left shift quotient by 1
				ADD r3, r3, #1			; Add 1 to Quotient (set LSB of Quotient to 1)

div_yes			MOV r1, r1, LSR #1		; Logical right shift divisor by 1

				CMP r2, #0				; compare counter in r2 to 0
				BLE div_endFunc			; if counter <= 0, goto label "endFunc", as we are done
				SUB r2, r2, #1			; decrement counter by 1 (r2 := r2 - 1)
				BGT	div_loop			; goto label loop if counter is greater than 0

div_endFunc

				CMP r5, #0				; Compare value in r5 to 0. A 0 tells us that we
										; need to negate(take two's complement of) the quotient
										; before returning it to the caller, whereas a 1 in r5
										; tells us that no negation is needed.
				BNE div_done			; if r5 != 0, goto label "done" (because r5 == 1 means
										; no negation needed).
				MVN r3, r3				; Take 1's complement of quotient in register r3.
				ADD r3, r3, #1			; Add 1 to 1's complement to form 2's complement,
										; completing the negation of the quotient. (r3: = r3 + 1)
div_done
				MOV r0, r3				; set r0 to value of quotient  (r0 := r3)
				MOV r1, r4				; set r1 to value of remainder (r1 := r4)

				LDMFD r13!, {r2-r12, r14}
				BX lr      				; Return to the C program

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;UART_INIT;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

uart_init		STMFD SP!,{lr}			; Store register lr on stack


										; 8-bit word length, 1 stop bit, no parity
										; Disable break control
										; Enable divisor latch access
				LDR r4, =0xE000C00C
				MOV r0, #0x83
				STRB r0, [r4]

										; Set lower divisor latch for 9,600 baud
				LDR r4, =0xE000C000
				MOV r0, #0x78
				STRB r0, [r4]

										; Set upper divisor latch for 9,600 baud
				LDR r4, =0xE000C004
				MOV r0, #0x00
				STRB r0, [r4]

										; 8-bit word length, 1 stop bit, no parity,
										; Disable break control
										; Disable divisor latch access
				LDR r4, =0xE000C00C
				MOV r0, #0x03
				STRB r0, [r4]

				LDMFD sp!, {lr}
				BX lr

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



pin_connect_block_setup_for_uart0
				STMFD sp!, {r0, r1, lr}
				LDR r0, =0xE002C000  ; PINSEL0
				LDR r1, [r0]
				ORR r1, r1, #5
				BIC r1, r1, #0xA
				STR r1, [r0]
				LDMFD sp!, {r0, r1, lr}
				BX lr



				END
